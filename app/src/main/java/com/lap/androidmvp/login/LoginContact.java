package com.lap.androidmvp.login;

/**
 * Created by somchaiwaikoona on 20/4/2018 AD.
 */

public class LoginContact {
    interface View{
        void gotoMainAcitivy();
        void showUsernameError();
        void showPasswordError();
        void hideError(String type);
        void showDialogLoading();
        void hideDialogLoading();
    }
    interface ActiconListenner{
         void onBtnLoginClick(String username, String password);
    }
}
