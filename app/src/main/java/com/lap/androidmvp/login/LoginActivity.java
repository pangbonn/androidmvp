package com.lap.androidmvp.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.lap.androidmvp.MainActivity;
import com.lap.androidmvp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by somchaiwaikoona on 20/4/2018 AD.
 */

public class LoginActivity extends AppCompatActivity implements LoginContact.View , View.OnClickListener{

    @BindView(R.id.edUsername)
    EditText edUsername;
    @BindView(R.id.edPassword)
    EditText edPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.tvRegister)
    TextView tvRegister;

    LoginContact.ActiconListenner mActionListenner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        btnLogin.setOnClickListener(this);
        mActionListenner = new LoginPresenter(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                mActionListenner.onBtnLoginClick(edUsername.getText().toString().trim(),edPassword.getText().toString().trim());
                break;
        }
    }

    @Override
    public void gotoMainAcitivy() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void showUsernameError() {
        edUsername.setError("this Require");
    }


    @Override
    public void showPasswordError() {
        edPassword.setError("this Require");
    }

    @Override
    public void hideError(String type) {
        switch (type){
            case "useranme":
                edUsername.setError(null);
                break;
            case "password":
                edPassword.setError(null);
                break;
        }
    }

    @Override
    public void showDialogLoading() {
        //Todo
    }

    @Override
    public void hideDialogLoading() {
        //Todo
    }


}
