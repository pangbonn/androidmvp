package com.lap.androidmvp.login;

/**
 * Created by somchaiwaikoona on 20/4/2018 AD.
 */

public class LoginPresenter implements LoginContact.ActiconListenner {
    LoginContact.View mView;

    public LoginPresenter(LoginContact.View mView) {
        this.mView = mView;
    }


    @Override
    public void onBtnLoginClick(String username, String password) {
        if(username.isEmpty() || password.isEmpty()) {
            validateLogin(username, password);
        }else{
            mView.hideError("username");
            mView.hideError("password");
            mView.gotoMainAcitivy();
        }
    }

    public void validateLogin(String username, String password) {

        if (username.isEmpty())
            mView.showUsernameError();
        if (password.isEmpty())
            mView.showPasswordError();
    }
}
